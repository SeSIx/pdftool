from PyPDF2 import PdfFileReader, PdfFileWriter, PdfFileMerger
import PySimpleGUI as sg
import os 
from os import path
import getpass
import psutil



def lazy_merge(input_paths, output_path):
    pdf_merger = PdfFileMerger()
    for i in input_paths:
        pdf_merger.append(i)
    with open(output_path, "wb") as f_out:
        pdf_merger.write(f_out)
    pdf_merger.close() 

def split_pdf(begin,end,path,save_path,Output_path):
    path_list=[]   
    with open(path, 'rb') as f:
        pdf = PdfFileReader(f)
        pdf_len = pdf.getNumPages()
        if begin<1:
            sg.PopupError(f'PDF beginnt mit Seite 1')
            Split_Window(save_path,Output_path)
        elif end>pdf_len or begin>pdf_len:
            sg.PopupError(f'PDF hat nur {pdf_len} Seiten')
            Split_Window(save_path,Output_path)
        elif begin > end:
            sg.PopupError(f'\'Von:\' muss kleiner sein als \'bis:\'')
            Split_Window(save_path,Output_path)
        else:
            os.mkdir(save_path)
            for page in range(begin-1,end):
                writer = PdfFileWriter()
                writer.addPage(pdf.getPage(page))
                with open(save_path+f"\{page}.pdf", "wb") as f_out:
                    writer.write(f_out) 
                path_list.append(save_path+f"\{page}.pdf")
            lazy_merge(path_list,Output_path+"\{}-{}.pdf".format(begin,end))
            os.system("rmdir /s /q "+save_path)

def Split_Window(save_path,Output_path):
    num=['0','1','2','3','4','5','6','7','8','9']
    split_layout= [[sg.Text('Datei')], [sg.Input(), sg.FileBrowse(file_types=(("PDF\'s", "*.pdf"),))],[sg.Text("Von"), sg.Input(size=(3,2)),sg.Text("bis"), sg.Input(size=(3,2))], [sg.OK(), sg.Cancel()] ]
    split_window= sg.Window('PDF_Split', split_layout)
    while True:
        event, values,  = split_window.read(close=True)
        if event == sg.WIN_CLOSED:
            break
        else:
            input_valid=True 
            for i in values[1]:
                if i not in num:
                    input_valid=False
                    sg.PopupError(f'\'Von:\' darf nur Ziffern enthalten')
                    Split_Window(save_path,Output_path)
            for i in values[2]:
                if i not in num:
                    input_valid=False
                    sg.PopupError(f'\'bis:\' darf nur Ziffern enthalten')
                    Split_Window(save_path,Output_path)
            if input_valid==True:
                if os.path.isfile(values[0]):
                    split_pdf(int(values[1]),int(values[2]),values[0],save_path,Output_path)
                else:
                    sg.PopupError(f'Die Datei existiert nicht.')
                    Split_Window(save_path,Output_path)



def Merge_Window(Output_path):
    merge_layout= [[sg.Text('Filename')], [sg.Input(), sg.FileBrowse(file_types=(("PDF\'s", "*.pdf"),))], [sg.Input(), sg.FileBrowse()], [sg.OK(), sg.Cancel()] ]
    merge_window= sg.Window('PDF_Merge', merge_layout)
    while True:
        event, values,  = merge_window.read(close=True)
        if event == sg.WIN_CLOSED:
            break
        elif event == 'Cancel':
            break
        else:
            if os.path.isfile(str(values[0])) == True and os.path.isfile(str(values[1])) == True:
                lazy_merge([values[0],values[1]],Output_path+"\\merged.pdf")
            else:
                sg.PopupError(f'Eine Datei existiert nicht.')
                Merge_Window(Output_path)

def main():
    user=getpass.getuser()
    PDF_Tool_Path=r"C:\\Users\\"+user+"\Desktop\PDF_Tool"
    PDF_Tool_exist = os.path.exists(PDF_Tool_Path)
    Output_path =r"C:\\Users\\"+user+"\Desktop\PDF_Tool\\Output"
    Output_exist = os.path.exists(Output_path)
    if PDF_Tool_exist == False:
        os.mkdir(r"C:\\Users\\"+user+"\Desktop\PDF_Tool")
    if Output_exist == False:
        os.mkdir(r"C:\\Users\\"+user+"\Desktop\PDF_Tool\\Output")
    save_path=r"C:\\Users\\"+user+"\Desktop\PDF_Tool\\PDF_Tool_tmp"
    layout = [[sg.Text("Was möchtest du machen?")] ,[sg.Button("PDFMerge"), sg.Button("PDFSplit")]]
    window = sg.Window("type", layout)
    while True:
            event, values,  = window.read(close=True)
            if event == sg.WIN_CLOSED:
                break
            elif event=="PDFMerge":
                Merge_Window(Output_path)
            elif event=="PDFSplit":
                Split_Window(save_path,Output_path)
                   
if __name__ == "__main__":
    main()